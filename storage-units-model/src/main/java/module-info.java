/*
 * SPDX-FileCopyrightText: The Storage-Units Authors
 * SPDX-License-Identifier: 0BSD
 */
/**
 * Module for the core data model.
 */
@org.jspecify.annotations.NullMarked
module wtf.metio.storageunits.model {

    requires org.jetbrains.annotations;
    requires com.github.spotbugs.annotations;
    requires org.jspecify;

    exports wtf.metio.storageunits.model;

}
